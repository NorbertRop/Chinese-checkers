package TP;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class Board {

	protected static final int WIDTH = 13, HEIGHT = 19;   //old = 17
	protected static final int[] WIDTHS3 = {3, 5, 7, 9, 27, 29, 27, 25, 23, 21, 23, 25, 27, 29, 27, 9, 7, 5, 3};    //tablica 3.0 z przerwami i konturem
	private int[] colors;
	public static HashMap<Integer, String> colorMap;
	private static HashMap<Integer, ArrayList<Point>> winMap;

	protected final int[][] theBoard;

	public Board() {
		theBoard = new int[HEIGHT][];
		for (int i = 0; i < HEIGHT; i++) {
			theBoard[i] = new int[WIDTHS3[i]];
		}

		Color homeColor = Color.RED;
		initBoard();
		colorMap = new HashMap<Integer, String>();
		colorMap.put(1, "RED");
		colorMap.put(2, "PINK");
		colorMap.put(3, "BLUE");
		colorMap.put(4, "GREEN");
		colorMap.put(5, "YELLOW");
		colorMap.put(6, "ORANGE");

		winMap = new HashMap<Integer, ArrayList<Point>>();
		ArrayList<Point> points = new ArrayList<Point>();
		int j, k = 14;

		for (int i = 1; i < 5; i++) {
			j = k--;
			for (; j < k  + 2 * i ; j += 2) {
				points.add(new Point(i, j));
			}
		}
		winMap.put(1, points);
		points = new ArrayList<Point>();

		for (int i = 26; i > 19; i -= 2) {
			points.add(new Point(5, i));
		}
		for (int i = 25; i > 20; i -= 2) {
			points.add(new Point(6, i));
		}
		for (int i = 24; i > 21; i -= 2) {
			points.add(new Point(7, i));
		}
		points.add(new Point(8, 23));

		winMap.put(2, points);
		points = new ArrayList<Point>();

		for (int i = 26; i > 19; i -= 2) {
			points.add(new Point(13, i));
		}
		for (int i = 25; i > 20; i -= 2) {
			points.add(new Point(12, i));
		}
		for (int i = 24; i > 21; i -= 2) {
			points.add(new Point(11, i));
		}
		points.add(new Point(10, 23));

		winMap.put(3, points);
		points = new ArrayList<Point>();

		points.add(new Point(17, 14));
		for (int i = 11; i < 18; i += 2) {
			points.add(new Point(14, i));
		}
		for (int i = 12; i < 17; i += 2) {
			points.add(new Point(15, i));
		}
		for (int i = 13; i < 16; i += 2) {
			points.add(new Point(16, i));
		}

		winMap.put(4, points);
		points = new ArrayList<Point>();

		for (int i = 8; i > 1; i -= 2) {
			points.add(new Point(13, i));
		}
		for (int i = 7; i > 2; i -= 2) {
			points.add(new Point(12, i));
		}
		for (int i = 6; i > 3; i -= 2) {
			points.add(new Point(11, i));
		}
		points.add(new Point(10, 5));

		winMap.put(5, points);
		points = new ArrayList<Point>();

		for (int i = 8; i > 1; i -= 2) {
			points.add(new Point(5, i));
		}
		for (int i = 7; i > 2; i -= 2) {
			points.add(new Point(6, i));
		}
		for (int i = 6; i > 3; i -= 2) {
			points.add(new Point(7, i));
		}
		points.add(new Point(8, 5));

		winMap.put(6, points);
	}

	/**
	 * Initialize the board array with the following values:
	 * -2  -> invisible outline
	 * -1  -> gaps between adjacent pieces
	 * 0   -> empty fields
	 * 1-6 -> all 6 colours of the pieces
	 */
	public abstract void initBoard();

	/**
	 * Mark player's move in the board.
	 *
	 * @param from field on which the chosen piece currently is
	 * @param to   field to which the piece is to be moved
	 */
	public void changeBoard(Point from, Point to) {
		int temp;

		temp = theBoard[from.getX()][from.getY()];
		theBoard[from.getX()][from.getY()] = 0;
		theBoard[to.getX()][to.getY()] = temp;
	}

	public int[] getWidths() {
		return WIDTHS3;
	}

	public int[][] getTheBoard() {
		return theBoard;
	}

	public void printArray() {
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTHS3[i]; j += 1) {
				System.out.print(theBoard[i][j] + " | ");
			}
			System.out.println();
		}
	}

	public int[] getColors() {
		return colors;
	}

	public static HashMap<Integer, String> getColorMap() {
		return colorMap;
	}

	public static ArrayList<Point> getWinPositions(int color){
		return winMap.get(color);
	}

	public static Point getArrayPoint(Point p) {
		int x1 = p.getX(), y1 = p.getY();
		if (x1 == 1 || x1 == 17) {
			y1 -= 12;
		}
		if (x1 == 2 || x1 == 16) {
			y1 -= 11;
		}
		if (x1 == 3 || x1 == 15) {
			y1 -= 10;
		}
		if (x1 == 4 || x1 == 6 || x1 == 12 || x1 == 14) {
			y1 -= 1;
		}
		if (x1 == 7 || x1 == 11) {
			y1 -= 2;
		}
		if (x1 == 8 || x1 == 10) {
			y1 -= 3;
		}
		if (x1 == 9) {
			y1 -= 4;
		}
		return new Point(x1, y1);
	}

	public static int getStarPoint(Point p) {
		int x1 = p.getX(), y1 = p.getY();
		if (x1 == 1 || x1 == 17) {
			y1 += 12;
		}
		if (x1 == 2 || x1 == 16) {
			y1 += 11;
		}
		if (x1 == 3 || x1 == 15) {
			y1 += 10;
		}
		if (x1 == 4 || x1 == 6 || x1 == 12 || x1 == 14) {
			y1 += 1;
		}
		if (x1 == 7 || x1 == 11) {
			y1 += 2;
		}
		if (x1 == 8 || x1 == 10) {
			y1 += 3;
		}
		if (x1 == 9) {
			y1 += 4;
		}
		return y1;
	}
}