package TP;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;

public class ControllerTest extends Controller {
	private Socket requestSocket;
	private ObjectOutputStream out;
	private ObjectInputStream in;

	public ControllerTest() {}

	@Override
	public void connect() {
		try {
			requestSocket = new Socket("localhost", 2004);
			System.out.println("Connected to localhost in port 2004");
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream());
			//Sending login name
			out.writeObject(new Random().nextInt());
			out.flush();
			synchronized (this) {
				this.wait(100);
			}
			out.writeObject("1");
			out.writeObject("bot");
		} catch (IOException e) {
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
