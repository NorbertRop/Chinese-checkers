package TP;

import java.awt.*;

public class App {
	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable(){
			public void run() {
				Controller controller = new Controller(new ClientView(), new Model());
			}
		});
	}
}
