package TP.Server;

import TP.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

/**
 * Creating new thread for every client and communicate with him
 *
 * @see Thread
 */

public class SocketThread extends Thread {

	private Socket socket;
	String message;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	ArrayList<ServerGameSession> rooms;
	Map<String, ObjectOutputStream> map;
	ArrayList<String> players;
	protected ServerGameSession actualGame;
	ArrayList<Bot> bots;
	String clientNick;
	private Random random;
	private boolean started = false;
	ArrayList<Point> possibleMoves;
	protected int color;

	protected SocketThread() {

	}

	public SocketThread(ServerGameSession session, int color) {
		possibleMoves = new ArrayList<Point>();
		actualGame = session;
		this.color = color;
		bots = new ArrayList<Bot>();
	}

	SocketThread(Socket socket, ArrayList<ServerGameSession> games, Map<String,
			ObjectOutputStream> map, ArrayList<String> players, ArrayList<Bot> bots) {
		this.socket = socket;
		this.rooms = games;
		this.map = map;
		this.random = new Random();
		this.players = players;
		possibleMoves = new ArrayList<Point>();
		this.bots = bots;
	}

	@Override
	public void run() {
		try {
			in = new ObjectInputStream(socket.getInputStream());
			out = new ObjectOutputStream(socket.getOutputStream());

			if (checkExit()) {
				if (players.contains(message) || message.equals("")) {
					sendMessage("n");
				} else {
					clientNick = message;
					map.put(message, out);
					players.add(message);
					sendMessage("y");
					for (ServerGameSession g : rooms) {
						out.writeObject(g.getDescription());
					}
					out.flush();
				}
			}

			do {
				if (checkExit()) {
					actualGame = rooms.get(Integer.valueOf(message));
					if (actualGame.getSize() > actualGame.getPlayersNick().size()) {
						sendMessage("i");
						actualGame.addPlayer(clientNick);
						out.writeObject(actualGame.getPlayersNick());
						break;
					} else {
						sendMessage("o");
					}
				}
			} while (true);

			sendOthers(clientNick);

			if (actualGame.getReadyPlayers() == actualGame.getSize()) {
				getReady();
				actualGame.setTurn(actualGame.getPlayersNick().get(random.nextInt(actualGame.getSize())));
				sleep(5000);
				startGame();
				sendTurn();
			}

			do {
				if (checkExit()) {
					System.out.println("Server received - " + message);
					String[] args = message.split(" ");
					String type = args[0];
					if (type.equals("chat")) {
						System.out.println("Sending " + clientNick + " " + message.substring(5, message.length()));
						sendAllChat(clientNick + ": " + message.substring(5, message.length()));
					} else if (type.equals("move")) {
						Point from = new Point(Integer.valueOf(args[1]), Integer.valueOf(args[2]));
						Point to = new Point(Integer.valueOf(args[3]), Integer.valueOf(args[4]));
						if (clientNick.equals(actualGame.getTurn())) {
							if (checkMove(from, to) && started) {
								actualGame.nextPlayer();
								doMove(from, to);
								sendAll("move y " + args[1] + " " + args[2] + " " + args[3] + " " + args[4]);
								sendTurn();
								if (checkWin()) {
									started = false;
									sendAll("end " + clientNick);
								}
							} else {
								sendMessage("move n");
							}
						} else {
							sendMessage("move turn");
						}
					} else if (type.equals("ready")) {
						createBoard();
						color = actualGame.registerColor(clientNick);
						synchronized (this) {
							this.wait(500);
						}
						sendChatMessage("INFO You are " + Board.getColorMap().get(color) + " color");
						sendChatMessage("INFO Your nick " + clientNick);
						started = true;
					} else if (type.equals("bot")) {
						if (!started) {
							System.out.println("Tworze bota");
							Bot b = new Bot(rooms, map, players, actualGame, bots);
							bots.add(b);
							actualGame.addPlayer(b.getNick());
							sendAll(b.getNick());
							b.start();
							if (actualGame.getReadyPlayers() == actualGame.getSize()) {
								getReady();
								actualGame.setTurn(actualGame.getPlayersNick().get(random.nextInt(actualGame.getSize())));
								this.sleep(5000);
								startGame();
								sendTurn();
							}
						}
					} else if (type.equals("skip")) {
						if (started) {
							for (Bot b : bots) {
								synchronized (b) {
									b.unlock();
									b.getMessage("skip");
									b.lock();
								}
							}
							if (actualGame.getTurn().equals(clientNick)) {
								actualGame.nextPlayer();
								sendTurn();
							} else {
								sendChatMessage("INFO It's not your turn!");
							}
						}
					}
				} else {
					break;
				}
			} while (true);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.out.println("Wrong number");
			deletePlayer();
		}
	}

	/**
	 * Send message to client
	 *
	 * @param msg message that we want to send
	 */
	private void sendMessage(String msg) {
		if (!socket.isClosed()) {
			try {
				out.writeObject(msg);
				out.flush();
				System.out.println("Server send - " + msg);
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	private void sendOthers(String msg) {
		if (!socket.isClosed()) {
			for (Map.Entry<String, ObjectOutputStream> entry : map.entrySet()) {
				if (actualGame.getPlayersNick().contains(entry.getKey()) && !entry.getKey().equals(clientNick)) {
					try {
						entry.getValue().writeObject(msg);
					} catch (IOException e) {
						deletePlayer();
						e.printStackTrace();
					}
				}
			}
			for (Bot b : bots) {
				if (actualGame.getPlayersNick().contains(b.getNick()) && !b.getNick().equals(clientNick)) {
					synchronized (b) {
						b.unlock();
						b.getMessage(msg);
						b.lock();
					}
				}
			}
		}

	}

	private void sendAll(String msg) {
		if (!socket.isClosed()) {
			for (Map.Entry<String, ObjectOutputStream> entry : map.entrySet()) {
				if (actualGame.getPlayersNick().contains(entry.getKey())) {
					try {
						entry.getValue().writeObject(msg);
					} catch (IOException e) {
						deletePlayer();
						e.printStackTrace();
					}
				}
			}
			for (Bot b : bots) {
				if (actualGame.getPlayersNick().contains(b.getNick())) {
					synchronized (b) {
						b.unlock();
						b.getMessage(msg);
						b.lock();
					}
				}
			}
		}

	}

	private void sendAllChat(String msg) {
		if (!socket.isClosed()) {
			for (Map.Entry<String, ObjectOutputStream> entry : map.entrySet()) {
				if (actualGame.getPlayersNick().contains(entry.getKey())) {
					try {
						entry.getValue().writeObject("chat " + msg);
					} catch (IOException e) {
						deletePlayer();
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void sendTurn() {
		if (!socket.isClosed()) {
			for (Map.Entry<String, ObjectOutputStream> entry : map.entrySet()) {
				if (actualGame.getPlayersNick().contains(entry.getKey())) {
					try {
						entry.getValue().writeObject("chat INFO " + actualGame.getTurn() + " turn \n");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void sendChatMessage(String msg) {
		if (!socket.isClosed()) {
			try {
				out.writeObject("chat " + msg + "\n");
				out.flush();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	private void getReady() {
		if (!socket.isClosed()) {
			for (Map.Entry<String, ObjectOutputStream> entry : map.entrySet()) {
				try {
					if (actualGame.getPlayersNick().contains(entry.getKey())) {
						entry.getValue().writeObject("ready");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			for (Bot b : bots) {
				if (actualGame.getPlayersNick().contains(b.getNick())) {
					synchronized (b) {
						b.unlock();
						b.getMessage("ready");
						b.lock();
					}
				}
			}
		}
	}

	private void startGame() {
		for (Map.Entry<String, ObjectOutputStream> entry : map.entrySet()) {
			if (actualGame.getPlayersNick().contains(entry.getKey())) {
				try {
					String start;

					switch (actualGame.getSize()) {
						case 2:
							start = "start 2";
							break;
						case 3:
							start = "start 3";
							break;
						case 4:
							start = "start 4";
							break;
						case 6:
							start = "start 6";
							break;
						default:
							start = "start 6";
							break;
					}

					entry.getValue().writeObject(start);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	void createBoard() {
		switch (actualGame.getSize()) {
			case 2:
				actualGame.setBoard(new BoardTwo());
				break;
			case 3:
				actualGame.setBoard(new BoardThree());
				break;
			case 4:
				actualGame.setBoard(new BoardFour());
				break;
			case 6:
				actualGame.setBoard(new BoardSix());
				break;
			default:
				actualGame.setBoard(new BoardSix());
				break;
		}
	}

	private boolean checkExit() {
		if (!socket.isClosed()) {
			try {
				message = String.valueOf(in.readObject());
				if (message == null || message.equalsIgnoreCase("bye")) {
					System.out.println("Closing connection");
					if (actualGame != null) {
						deletePlayer();
					}
					in.close();
					out.close();
					socket.close();
					return false;
				}
			} catch (IOException e) {
				System.out.println("Couldn't close connection");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	void deletePlayer() {
		ArrayList<Bot> toRemove = new ArrayList<Bot>();
		boolean removeBots = true;
		if (players.contains(clientNick)) {
			players.remove(clientNick);
			map.remove(clientNick);
			actualGame.removePlayerNick(clientNick);
			actualGame.getPlayersColor().remove(clientNick);
			synchronized (this) {
				actualGame.decrement();
			}
			for (String nick : actualGame.getPlayersNick()) {
				if (players.contains(nick)) {
					removeBots = false;
					break;
				}
			}

			if (removeBots) {
				for (Bot b : bots) {
					if (actualGame.getPlayersNick().contains(b.getNick())) {
						actualGame.removePlayerNick(b.getNick());
						actualGame.getPlayersColor().remove(b.getNick());
						actualGame.decrement();
						toRemove.add(b);
					}
				}
			}

			for (Bot b : toRemove) {
				bots.remove(b);
			}
		}
	}

	/**
	 * Check if given move is correct.
	 *
	 * @return true if the move is correct, false otherwise
	 */
	protected boolean checkMove(Point from, Point to) {
		possibleMoves.clear();
		int x1 = from.getX(), y1, x2 = to.getX(), y2;
		int pieceColor;

		y1 = Board.getStarPoint(from);

		y2 = Board.getStarPoint(to);

		pieceColor = actualGame.getValue(x1, y1);

		if (color != pieceColor) {
			return false;
		}

		canJump(new Point(x1, y1));

		if (actualGame.getValue(x1 + 1, y1 + 1) == 0) {
			possibleMoves.add(new Point(x1 + 1, y1 + 1));
		}
		if (actualGame.getValue(x1 + 1, y1 - 1) == 0) {
			possibleMoves.add(new Point(x1 + 1, y1 - 1));
		}
		if (actualGame.getValue(x1 - 1, y1 + 1) == 0) {
			possibleMoves.add(new Point(x1 - 1, y1 + 1));
		}
		if (actualGame.getValue(x1 - 1, y1 - 1) == 0) {
			possibleMoves.add(new Point(x1 - 1, y1 - 1));
		}
		if (actualGame.getValue(x1, y1 + 2) == 0) {
			possibleMoves.add(new Point(x1, y1 + 2));
		}
		if (actualGame.getValue(x1, y1 - 2) == 0) {
			possibleMoves.add(new Point(x1, y1 - 2));
		}

		return contains(new Point(x2, y2));
	}

	void doMove(Point from, Point to) {
		actualGame.getBoard().changeBoard(from, to);
	}

	private void canJump(Point p) {
		int x = p.getX(), y = p.getY();
		Point r;

		if (actualGame.getValue(x + 1, y + 1) > 0 && actualGame.getValue(x + 2, y + 2) == 0 && !contains(new Point(x + 2, y + 2))) {
			r = new Point(x + 2, y + 2);
			possibleMoves.add(r);
			canJump(r);
		}
		if (actualGame.getValue(x + 1, y - 1) > 0 && actualGame.getValue(x + 2, y - 2) == 0 && !contains(new Point(x + 2, y - 2))) {
			r = new Point(x + 2, y - 2);
			possibleMoves.add(r);
			canJump(r);
		}
		if (actualGame.getValue(x - 1, y + 1) > 0 && actualGame.getValue(x - 2, y + 2) == 0 && !contains(new Point(x - 2, y + 2))) {
			r = new Point(x - 2, y + 2);
			possibleMoves.add(r);
			canJump(r);
		}
		if (actualGame.getValue(x - 1, y - 1) > 0 && actualGame.getValue(x - 2, y - 2) == 0 && !contains(new Point(x - 2, y - 2))) {
			r = new Point(x - 2, y - 2);
			possibleMoves.add(r);
			canJump(r);
		}
		if (actualGame.getValue(x, y + 2) > 0 && actualGame.getValue(x, y + 4) == 0 && !contains(new Point(x, y + 4))) {
			r = new Point(x, y + 4);
			possibleMoves.add(r);
			canJump(r);
		}
		if (actualGame.getValue(x, y - 2) > 0 && actualGame.getValue(x, y - 4) == 0 && !contains(new Point(x, y - 4))) {
			r = new Point(x, y - 4);
			possibleMoves.add(r);
			canJump(r);
		}
	}

	private boolean contains(Point point) {
		for (Point p : possibleMoves) {
			if (p.getX() == point.getX() && p.getY() == point.getY()) {
				return true;
			}
		}
		return false;
	}

	protected boolean checkWin() {
		ArrayList<Point> win = Board.getWinPositions(color);
		for (Point p : win) {
			if (actualGame.getValue(p.getX(), p.getY()) != color) {
				return false;
			}
		}
		return true;
	}

	public void changeColor(int color) {
		this.color = color;
	}

	public int getColor() {
		return color;
	}
}
