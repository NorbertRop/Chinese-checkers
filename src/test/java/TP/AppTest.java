package TP;

import TP.Server.Server;

import static org.junit.Assert.*;

import TP.Server.ServerGameSession;
import TP.Server.SocketThread;
import org.junit.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AppTest {
	Server server;
	ServerGameSession session;
	Board board;
	SocketThread socketThread;
	Method method;

	@Test
	public void connectionCheck() throws InterruptedException {
		server = new Server();
		Thread a = new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						callPrivateMethod(server, "run", null, null);
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
		});
		a.start();
		Thread b = new Thread(new Runnable() {
			public void run() {
				ControllerTest controller = new ControllerTest();
				controller.connect();
			}
		});
		b.start();
		Thread c = new Thread(new Runnable() {
			public void run() {
				synchronized (this) {
					try {
						this.wait(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				ControllerTest controller = new ControllerTest();
				controller.connect();
			}
		});
		c.start();
		synchronized (this) {
			this.wait(2000);
		}

		assertEquals(2, server.getNumberOfPlayers());
	}

	@Test
	public void checkMoveTest() throws NoSuchMethodException {
		boolean b;
		session = new ServerGameSession("Test", 6);
		board = new BoardSix();
		session.setBoard(board);

		socketThread = new SocketThread(session, 1);
		method = socketThread.getClass().getDeclaredMethod("checkMove", new Class[]{Point.class, Point.class});
		method.setAccessible(true);

		b = doMove(15, 6, 13, 14);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 8, 13, 10);
		assertEquals("Not possible to move", true, b);
		b = doMove(6, 4, 6, 8);
		assertEquals("Not possible to move", true, b);
		b = doMove(4, 14, 5, 14);
		assertEquals("Not possible to move", true, b);
		b = doMove(6, 20, 6, 18);
		assertEquals("Not possible to move", true, b);
		b = doMove(12, 22, 10, 18);
		assertEquals("Not possible to move", true, b);
		b = doMove(15, 2, 13, 18);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 4, 13, 16);
		assertEquals("Not possible to move", true, b);
		b = doMove(7, 4, 7, 6);
		assertEquals("Not possible to move", true, b);
		b = doMove(3, 2, 5, 10);
		assertEquals("Not possible to move", true, b);
		b = doMove(8, 20, 6, 16);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 22, 11, 18);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 18, 12, 16);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 2, 13, 4);
		assertEquals("Not possible to move", true, b);
		b = doMove(7, 2, 9, 2);
		assertEquals("Not possible to move", true, b);
		b = doMove(5, 10, 6, 10);
		assertEquals("Not possible to move", true, b);
		b = doMove(6, 22, 8, 18);
		assertEquals("Not possible to move", true, b);
		b = doMove(11, 20, 11, 16);
		assertEquals("Not possible to move", true, b);
		b = doMove(16, 2, 15, 2);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 16, 13, 18);
		assertEquals("Not possible to move", true, b);
		b = doMove(5, 8, 7, 16);
		assertEquals("Not possible to move", true, b);
		b = doMove(6, 10, 7, 10);
		assertEquals("Not possible to move", true, b);
		b = doMove(6, 18, 6, 14);
		assertEquals("Not possible to move", true, b);
		b = doMove(11, 22, 7, 14);
		assertEquals("Not possible to move", true, b);
		b = doMove(16, 4, 15, 6);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 18, 13, 22);
		assertEquals("Not possible to move", true, b);
		b = doMove(7, 6, 7, 8);
		assertEquals("Not possible to move", true, b);
		b = doMove(4, 12, 5, 12);
		assertEquals("Not possible to move", true, b);
		b = doMove(7, 20, 9, 16);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 26, 9, 14);
		assertEquals("Not possible to move", true, b);
		b = doMove(14, 12, 8, 16);
		assertEquals("Not possible to move", true, b);
		b = doMove(13, 6, 11, 6);
		assertEquals("Not possible to move", true, b);
		b = doMove(8, 2, 12, 8);
		assertEquals("Not possible to move", true, b);
		b = doMove(4, 10, 8, 8);
		assertEquals("Not possible to move", true, b);
	}

	private <P> Object callPrivateMethod(Object css, String methodName, Object obj, Class<P> objType) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		if (obj == null) {
			Method method = css.getClass().getDeclaredMethod(methodName);
			method.setAccessible(true);
			return method.invoke(css);
		} else {
			Method method = css.getClass().getDeclaredMethod(methodName, objType);
			method.setAccessible(true);
			return method.invoke(css, objType.cast(obj));
		}
	}

	private boolean doMove(int x1, int y1, int x2, int y2) {
		Object check = true;
		try {
			check = method.invoke(socketThread, new Point(x1, y1), new Point(x2, y2));
			board.changeBoard(new Point(x1, y1), new Point(x2, y2));
			if (socketThread.getColor() == 6) {
				socketThread.changeColor(1);
			} else {
				socketThread.changeColor(socketThread.getColor() + 1);
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return (Boolean) check;
	}

	private void doMove2(int x1, int y1, int x2, int y2) {
		board.changeBoard(new Point(x1, y1), new Point(x2, y2));
	}

	@Test
	public void getValueTest(){
		Point p = new Point(1, 2);
		int y = Board.getStarPoint(p);
		assertEquals(14, y);
		p = new Point(1, y);
		p = Board.getArrayPoint(p);
		assertEquals(1, p.getX());
		assertEquals(2, p.getY());

		p = new Point(5, 26);
		y = Board.getStarPoint(p);
		assertEquals(26, y);
		p = new Point(5, y);
		p = Board.getArrayPoint(p);
		assertEquals(5, p.getX());
		assertEquals(26, p.getY());

		p = new Point(9, 4);
		y = Board.getStarPoint(p);
		assertNotEquals(4, y);
		p = new Point(9, y);
		p = Board.getArrayPoint(p);
		assertNotEquals(5, p.getY());
	}

	@Test
	public void testServerGameSession(){
		ServerGameSession session = new ServerGameSession("Test", 4);
		session.setBoard(new BoardFour());
		session.addPlayer("Test");
		session.registerColor("Test");
		session.addPlayer("Test2");
		session.registerColor("Test2");

		session.setTurn("Test");
		session.nextPlayer();
		assertEquals("Test2", session.getTurn());

		Point p = new Point(1, 2);
		p = new Point(1, Board.getStarPoint(p));

		assertEquals(0, session.getValue(p.getX(), p.getY()));

		session.setBoard(new BoardTwo());
		assertEquals(4, session.getValue(p.getX(), p.getY()));
		session.setBoard(new BoardThree());
		assertEquals(0, session.getValue(p.getX(), p.getY()));
	}

	@Test
	public void testWin() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		SocketThreadTest thread = new SocketThreadTest(new BoardTwo());
		Object obj = callPrivateMethod(thread, "checkWin", null, null);
		boolean b = (Boolean) obj;

		assertEquals(false, b);
		board = new BoardSix();

		doMove2(4, 10, 14, 10);
		doMove2(4, 12, 14, 12);
		doMove2(4, 14, 14, 14);
		doMove2(4, 16, 14, 16);
		doMove2(3, 2, 15, 2);
		doMove2(3, 4, 15, 4);
		doMove2(3, 6, 15, 6);
		doMove2(2, 2, 16, 2);
		doMove2(2, 4, 16, 4);
		doMove2(1, 2, 17, 2);

		thread = new SocketThreadTest(board);
		obj = callPrivateMethod(thread, "checkWin", null, null);
		b = (Boolean) obj;
		assertEquals(true, b);
	}
}
