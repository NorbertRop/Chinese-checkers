package TP;

import javax.swing.*;

public class Model {
	private String nick;
	private ClientGameSession game;
	private DefaultListModel listModel;

	Model(){
		listModel = new DefaultListModel();
	}

	public void setGameSession(ClientGameSession g){
		game = g;
	}

	public DefaultListModel getListModel(){
		return listModel;
	}

	public ClientGameSession getGame() {
		return game;
	}

	public void setNick(String nick){
		this.nick = nick;
	}
}
