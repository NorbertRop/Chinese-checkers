package TP;

public class BoardThree extends Board {

    /**
     * Board for 3 players.
     */

    private int[] colors;

    public BoardThree(){
        super();
        this.colors = new int[]{1, 3, 5};
    }

    public int[] getColors() { return this.colors; }

    public void initBoard(){

        int kolorowe = 4;
        int puste = 5;
        int a, b;

        theBoard[0][0] = -2; theBoard[0][1] = -1; theBoard[0][2] = -2;  //kontur - przerwa - kontur

        for(int i = 1; i < 5; i++){

            theBoard[i][0] = -2; theBoard[i][1] = -1;

            if(i != 4){
                for(int j = 2; j < WIDTHS3[i] - 1; j += 2)
                    theBoard[i][j] = 0;
            }
            else{
                for(int j = 2; j < 10; j += 2)
                    theBoard[i][j] = -2;
                for(int j = 10; j < 18; j += 2)
                    theBoard[i][j] = 0;
                for(int j = 18; j < 26; j += 2)
                    theBoard[i][j] = -2;
            }

            theBoard[i][WIDTHS3[i] - 2] = -1; theBoard[i][WIDTHS3[i] - 1] = -2;

        }

        for(int i = 5; i < 10; i++){

            theBoard[i][0] = -2; theBoard[i][1] = -1;

            a = kolorowe;
            b = 0;
            for(int j = 2; j < WIDTHS3[i] - 1; j += 2){
                if(a > 0) {
                    theBoard[i][j] = 3;
                    a--;
                }
                else if(b < puste) {
                    theBoard[i][j] = 0;
                    b++;
                }
                else theBoard[i][j] = 5;
            }
            kolorowe--;
            puste++;

            theBoard[i][WIDTHS3[i] - 2] = -1; theBoard[i][WIDTHS3[i] - 1] = -2;
        }

        kolorowe = 1;
        puste = 8;
        for(int i = 10; i < 14; i++){

            theBoard[i][0] = -2; theBoard[i][1] = -1;

            a = kolorowe;
            b = 0;
            for(int j = 2; j < WIDTHS3[i] - 1; j += 2){
                if(a > 0) {
                    theBoard[i][j] = 0;
                    a--;
                }
                else if(b < puste) {
                    theBoard[i][j] = 0;
                    b++;
                }
                else theBoard[i][j] = 0;
            }
            kolorowe++;
            puste--;

            theBoard[i][WIDTHS3[i] - 2] = -1; theBoard[i][WIDTHS3[i] - 1] = -2;
        }

        for(int i = 14; i < HEIGHT - 1; i++){

            theBoard[i][0] = -2; theBoard[i][1] = -1;

            if(i != 14){
                for(int j = 2; j < WIDTHS3[i] - 1; j += 2)
                    theBoard[i][j] = 1;
            }
            else{
                for(int j = 2; j < 10; j += 2)
                    theBoard[i][j] = -2;
                for(int j = 10; j < 18; j += 2)
                    theBoard[i][j] = 1;
                for(int j = 18; j < 26; j += 2)
                    theBoard[i][j] = -2;
            }

            theBoard[i][WIDTHS3[i] - 2] = -1; theBoard[i][WIDTHS3[i] - 1] = -2;
        }

        theBoard[18][0] = -2; theBoard[18][1] = -1; theBoard[18][2] = -2;

        for(int i = 1; i < HEIGHT - 1; i++)
            for(int j = 1; j < WIDTHS3[i]; j += 2)
                theBoard[i][j] = -1;                            //przerwy oznaczone są przez -1
    }
}
