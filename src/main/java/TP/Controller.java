package TP;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class Controller {
	private ClientView clientView;
	private Model model;
	private Socket requestSocket;
	private ObjectOutputStream out;
	private ObjectInputStream in;

	Controller(ClientView v, Model m) {

		this.clientView = v;
		this.model = m;

		JList list = new JList(model.getListModel());
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		list.setVisibleRowCount(-1);
		list.addListSelectionListener(new RoomListener());
		clientView.setList(list);

		this.clientView.setUpGUI();

		this.clientView.pack();
		this.clientView.setSize(new Dimension(400, 300));
		this.clientView.setTitle("Checkers");
		this.clientView.setResizable(true);
		this.clientView.setLocationRelativeTo(null);
		this.clientView.setVisible(true);

		//Action listeners
		clientView.getConnectB().addActionListener(new ConnectionButtonListener());
		clientView.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		clientView.getChooseRoomB().addActionListener(new ChooseRoomListener());
		clientView.getChatInput().getActionMap().put("inputAction", new MessageAction());
		clientView.getSkip().addActionListener(new SkipListener());
		clientView.getAddBot().addActionListener(new AddBotListener());

		clientView.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				String ObjButtons[] = {"Yes", "No"};
				int PromptResult = JOptionPane.showOptionDialog(null, "Are you sure you want to exit?", "Exit current game", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, ObjButtons, ObjButtons[1]);
				if (PromptResult == JOptionPane.YES_OPTION) {
					try {
						if (requestSocket != null) {
							sendMessage("bye");
							in.close();
							out.close();
							requestSocket.close();
						}
					} catch (IOException ioException) {
						ioException.printStackTrace();
					}
					System.exit(0);
				}
			}
		});
	}

	Controller() { }

	/**
	 * Sending a message to server
	 *
	 * @param msg message that we want to send
	 */
	private void sendMessage(String msg) {
		try {
			if (!requestSocket.isClosed()) {
				out.writeObject(msg);
				out.flush();
				System.out.println("Client send - " + msg);
			} else {
				System.exit(1);
			}
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	protected void connect() {
		//Creating socket
		try {
			requestSocket = new Socket("localhost", 2004);
			System.out.println("Connected to localhost in port 2004");
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			out.flush();
			in = new ObjectInputStream(requestSocket.getInputStream());
			//Sending login name
			model.setNick(clientView.getLoginF().getText());
			out.writeObject(clientView.getLoginF().getText());
			out.flush();
			clientView.getLoginF().setText("");
			if (in.readObject().equals("y")) {
				for (int i = 0; i < 3; i++) {
					model.getListModel().addElement(in.readObject());
				}
				clientView.getConnectB().setEnabled(false);
			} else {
				JOptionPane.showMessageDialog(clientView, "Invalid nick or already used");
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(clientView, "Failed connection");
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(clientView, "Failed connection");
		}
	}

	/**
	 * Listen to button and connect to the server
	 *
	 * @see ActionListener
	 */
	class ConnectionButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			connect();
		}
	}

	class ChooseRoomListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JList list = clientView.getRoomList();
			if (requestSocket != null && list.getSelectedIndex() != -1) {
				sendMessage(String.valueOf(list.getSelectedIndex()));
				try {
					if ((in.readObject()).equals("i")) {
						clientView.getContentPane().removeAll();
						ArrayList<String> players = (ArrayList<String>) in.readObject();
						clientView.setRoom(players);
						clientView.validate();
						clientView.repaint();
						ChatThread attendant = new ChatThread();
						attendant.start();
					} else {
						JOptionPane.showMessageDialog(clientView, "Full room");
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	class RoomListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			JList list = clientView.getRoomList();
			if (list.getSelectedIndex() == -1) {
				//No selection, disable choose button.
				clientView.getChooseRoomB().setEnabled(false);

			} else {
				//Selection, enable the choose button.
				clientView.getChooseRoomB().setEnabled(true);
			}
		}
	}

	/**
	 * MouseListener for the board. Registers player's moves.
	 */
	class BoardListener implements MouseListener {

		private Point from;

		public void mousePressed(MouseEvent e) {
			for (Ellipse2D field : clientView.getBoardPanel().getFieldList()) {
				if (field.contains(e.getX(), e.getY())) {
					if (clientView.getBoardPanel().getClickCount() == 0) {
						//first click
						clientView.getBoardPanel().setHighlightIndex(clientView.getBoardPanel().getFieldList().indexOf(field));
						clientView.getBoardPanel().setClickCount(1);
						clientView.getBoardPanel().repaint();
						from = clientView.getBoardPanel().ellipseToIndex(field);
					} else if (clientView.getBoardPanel().getClickCount() == 1) {
						//second click
						if (clientView.getBoardPanel().getHighlightIndex() != clientView.getBoardPanel().getFieldList().indexOf(field)) {
							//if another piece was picked, make a move
							checkMove(from, clientView.getBoardPanel().ellipseToIndex(field));                    //TBD
						}
						clientView.getBoardPanel().setHighlightIndex(-1);            //remove highlight
						clientView.getBoardPanel().setClickCount(0);                //roll back the counter
						clientView.getBoardPanel().repaint();
					}
				}
			}
		}

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mouseClicked(MouseEvent e) {
		}

	}

	class MessageAction extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			try {
				out.writeObject("chat " + clientView.getChatInput().getText() + "\n");
				clientView.getChatInput().setText("");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	class ChatThread extends Thread {
		@Override
		public void run() {
			try {
				while (true) {
					String msg = String.valueOf(in.readObject());
					String[] args = msg.split(" ");
					String type = args[0];
					if (msg.startsWith("start")) {
						clientView.getContentPane().removeAll();

						int size = Integer.parseInt(msg.split(" ")[1]);
						switch (size) {
							case 2:
								model.setGameSession(new ClientGameSession(new BoardTwo()));
								break;
							case 3:
								model.setGameSession(new ClientGameSession(new BoardThree()));
								break;
							case 4:
								model.setGameSession(new ClientGameSession(new BoardFour()));
								break;
							case 6:
								model.setGameSession(new ClientGameSession(new BoardSix()));
								break;
							default:
								model.setGameSession(new ClientGameSession(new BoardSix()));
								break;
						}

						clientView.setBoard(model.getGame().getBoard());
						clientView.validate();
						clientView.repaint();
						clientView.getBoardPanel().addMouseListener(new BoardListener());
					} else if (type.equals("chat")) {
						if (args[1].equalsIgnoreCase("info")) {
							clientView.getChatArea().setForeground(Color.BLUE);
							clientView.getChatArea().append(msg.substring(5, msg.length()));
							clientView.scrollDown();
							clientView.getChatArea().setForeground(Color.BLACK);
						} else {
							clientView.getChatArea().append(msg.substring(5, msg.length()));
							clientView.scrollDown();
						}

					} else if (type.equals("move")) {
						if (args[1].equals("y")) {
							Point from = new Point(Integer.valueOf(args[2]), Integer.valueOf(args[3]));
							Point to = new Point(Integer.valueOf(args[4]), Integer.valueOf(args[5]));
							doMove(from, to);
						} else if (args[1].equals("n")) {
							JOptionPane.showMessageDialog(clientView, "Invalid move");
						} else {
							JOptionPane.showMessageDialog(clientView, "Not your turn");
						}
					} else if (msg.equals("ready")) {
						sendMessage("ready");
						clientView.getWaiting().setText("Get ready!");
						clientView.getWaiting().setForeground(Color.GREEN);
					} else if (type.equals("end")) {
						JOptionPane.showMessageDialog(clientView, args[1] + " won!!!");
					} else {
						clientView.getPlayersList().append(msg + "\n");
					}
				}
			} catch (IOException e) {
				System.out.println("Connection closed");
				JOptionPane.showMessageDialog(clientView, "Server closed");
				System.exit(1);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Let server check the move and if correct, return true.
	 *
	 * @param from field on which the chosen piece currently is
	 * @param to   field to which the player wishes to move the piece
	 */
	private void checkMove(Point from, Point to) {
		sendMessage("move " + String.valueOf(from.getX()) + " " + String.valueOf(from.getY()) + " " + String.valueOf(to.getX()) + " " + String.valueOf(to.getY()));
	}

	/**
	 * Draw the move on the board.
	 *
	 * @param from field on which the chosen piece currently is
	 * @param to   field to which the piece is to be moved
	 */
	private void doMove(Point from, Point to) {
		model.getGame().getBoard().changeBoard(from, to);
		clientView.getBoardPanel().setBoard(model.getGame().getBoard().getTheBoard());
		clientView.getBoardPanel().repaint();
	}

	class SkipListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			sendMessage("skip");
		}
	}

	class AddBotListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			sendMessage("bot");
		}
	}
}
