package TP.Server;

import TP.Board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ServerGameSession {
	private String description;
	private Board board;
	private int size;
	private ArrayList<String> playersNick;
	private String turn = null;
	private HashMap<String, Integer> playersColor;
	private int availableColor = 0;

	public ServerGameSession(String description, int size) {
		this.description = description;
		this.size = size;
		this.playersNick = new ArrayList<String>();
		playersColor = new HashMap<String, Integer>();
	}

	public String getDescription() {
		return description;
	}

	public int getSize() {
		return size;
	}

	public int getReadyPlayers() {
		return playersNick.size();
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public void addPlayer(String nick) {
		playersNick.add(nick);
	}

	public ArrayList<String> getPlayersNick() {
		return playersNick;
	}

	public int registerColor(String nick) {
		int col = 1;
		synchronized (this) {
			if (availableColor < size) {
				col = board.getColors()[availableColor];
			}
			availableColor++;
		}
		playersColor.put(nick, col);
		return col;
	}

	public void removePlayerNick(String nick) {
		if (playersNick.contains(nick)) {
			playersNick.remove(nick);
		}
	}

	public void setTurn(String nick) {
		if (playersNick.contains(nick)) {
			turn = nick;
		}
	}

	public String getTurn() {
		return turn;
	}

	public void nextPlayer() {
		int index = 0;
		int[] colors = board.getColors();
		for (int i = 0; i < colors.length; i++) {
			if (playersColor.get(turn) == colors[i]) {
				index = i;
				break;
			}
		}

		if (index + 1 == playersNick.size()) {
			index = 0;
		} else {
			index++;
		}


		for (Map.Entry<String, Integer> entry : playersColor.entrySet()) {
			if (entry.getValue() == colors[index]) {
				turn = entry.getKey();
				break;
			}
		}
	}

	public HashMap<String, Integer> getPlayersColor() {
		return playersColor;
	}

	public int getValue(int x, int y) {
		if (x == 0 || x == 18) {
			return -2;
		}
		if (x == 1 || x == 17) {
			y -= 12;
		}
		if (x == 2 || x == 16) {
			y -= 11;
		}
		if (x == 3 || x == 15) {
			y -= 10;
		}
		if (x == 4 || x == 6 || x == 12 || x == 14) {
			y -= 1;
		}
		if (x == 7 || x == 11) {
			y -= 2;
		}
		if (x == 8 || x == 10) {
			y -= 3;
		}
		if (x == 9) {
			y -= 4;
		}
		return board.getTheBoard()[x][y];
	}

	public void decrement() {
		availableColor--;
	}
}
