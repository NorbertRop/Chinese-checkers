package TP;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClientView extends JFrame {
	private JButton connectB, chooseRoomB;
	private JMenuBar menuBar;
	private JMenu options, help;
	private JMenuItem about;
	private JList availableRooms;
	private BoardPanel boardPanel;
	private JTextField loginF;
	private ChatPanel chatPanel;
	private JTextArea playersList;
	private JLabel waiting;
	private JMenuItem skip, addBot;

	private JPanel mainPane;

	private GridBagConstraints c;

	ClientView() {
		connectB = new JButton("Connect to server");
		chooseRoomB = new JButton("Choose room");
		menuBar = new JMenuBar();
		options = new JMenu("Options");
		help = new JMenu("Help");
		skip = new JMenuItem("Skip turn");
		addBot = new JMenuItem("Add a bot");
		about = new JMenuItem("About Checkers");

		chatPanel = new ChatPanel();

		mainPane = new JPanel(new GridBagLayout());

		c = new GridBagConstraints();

		setVisible(true);
	}

	/**
	 * Set up the GUI of the main screen.
	 */
	public void setUpGUI() {

		menuBar.add(options);
		menuBar.add(help);

		options.add(skip);
		options.add(addBot);
		help.add(about);

		setJMenuBar(menuBar);

		about.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Aplikacja \"Checkers\" pozwala graczowi doświadczyć niesamowitych wrażeń obecnych podczas rozgrywki w chińskie warcaby.\n\nAutorzy: Norbert Ropiak, Filip Janusz", "Informacje o grze Checkers", JOptionPane.INFORMATION_MESSAGE);
			}
		});

		loginF = new JTextField(15);
		loginF.setMinimumSize(new Dimension(100 , 20));
		c.gridx = 0; c.gridy = 0;
		c.insets = new Insets(10, 10, 10, 10);
		mainPane.add(loginF, c);

		c.gridx = 0; c.gridy = 1;
		c.gridwidth = 2; c.fill  = GridBagConstraints.BOTH; c.anchor = GridBagConstraints.CENTER;
		mainPane.add(connectB, c);

		JScrollPane scrollRooms = new JScrollPane(availableRooms);
		scrollRooms.setPreferredSize(new Dimension(150, 80));
		scrollRooms.setMinimumSize(new Dimension(150, 80));
		c.gridx = 0; c.gridy = 2; c.gridwidth = 1; c.fill = GridBagConstraints.NONE; c.anchor = GridBagConstraints.LINE_START;
		mainPane.add(scrollRooms, c);

		c.gridx = 1; c.gridy = 2;
		mainPane.add(chooseRoomB, c);

		add(mainPane);
	}

	/**
	 * Set up the GUI of the game screen.
	 *
	 * @param b the board array to be passed to the BoardPanel instance
	 */
	public void setBoard(Board b){

		setSize(new Dimension(800, 600));
		validate();

		setLayout(new GridBagLayout());
		c = new GridBagConstraints();

		boardPanel = new BoardPanel(b.getTheBoard(), b.getWidths(), this.getHeight(), 600);

		boardPanel.setPreferredSize(new Dimension(600, this.getHeight()));
		boardPanel.setMinimumSize(new Dimension(600, this.getHeight()));
		c.gridx = 0; c.gridy = 0; c.weightx = 1.0; c.weighty = 1.0; c.anchor = GridBagConstraints.CENTER;
		add(boardPanel, c);
		chatPanel.setPreferredSize(new Dimension(200, this.getHeight()));
		chatPanel.setMinimumSize(new Dimension(200, this.getHeight()));
		c.gridx = 1; c.anchor = GridBagConstraints.LINE_END; c.fill = GridBagConstraints.BOTH;
		add(chatPanel, c);

		//skip = new JButton("Skip turn");
		//c.gridx = 0; c.gridy = 1; c.anchor = GridBagConstraints.PAGE_END; c.weighty = 1.0;
		//add(skip, c);
	}

	/**
	 * Set up the GUI of the room's lobby.
	 *
	 * @param players list of players in this room
	 */
	public void setRoom(ArrayList<String> players){

		JPanel roomPanel = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();

		waiting = new JLabel("Waiting for players...", SwingConstants.CENTER);
		waiting.setFont(new Font("Arial", Font.BOLD, 30));
		waiting.setForeground(Color.RED);

		playersList = new JTextArea();
		playersList.setEditable(false);
		playersList.setText("");
		playersList.setFont(new Font("Arial", Font.BOLD, 20));
		JPanel playersListPanel = new JPanel(new GridBagLayout());
		playersListPanel.setPreferredSize(new Dimension(this.getWidth() / 2, this.getHeight() / 2));
		playersListPanel.setMinimumSize(new Dimension(this.getWidth() / 2, this.getHeight() / 2));
		c.weightx = 1.0; c.weighty = 1.0; c.fill = GridBagConstraints.BOTH;
		playersListPanel.add(playersList, c);
		for(String s : players){
			playersList.append(s+"\n");
		}

		chatPanel.setPreferredSize(new Dimension(this.getWidth() / 2, this.getHeight() / 2));
		chatPanel.setMinimumSize(new Dimension(this.getWidth() / 2, this.getHeight() / 2));

		TitledBorder titledBorder = BorderFactory.createTitledBorder("Players");
		playersListPanel.setBorder(titledBorder);
		titledBorder = BorderFactory.createTitledBorder("Chat");
		titledBorder.setTitleJustification(TitledBorder.RIGHT);
		chatPanel.setBorder(titledBorder);

		c.gridx = 0; c.gridy = 0; c.weightx = 0.0; c.weighty = 0.2; c.fill = GridBagConstraints.HORIZONTAL; c.gridwidth = 2;
		roomPanel.add(waiting, c);
		c.gridx = 0; c.gridy = 1; c.weightx = 0.5; c.weighty = 0.8; c.fill = GridBagConstraints.BOTH; c.gridwidth = 1;
		roomPanel.add(playersListPanel, c);
		c.gridx = 1; c.gridy = 1; c.weightx = 0.5; c.weighty = 0.8; c.fill = GridBagConstraints.BOTH;
		roomPanel.add(chatPanel, c);

		add(roomPanel);
	}

	public JButton getConnectB(){
		return connectB;
	}

	public void setList(JList list){
		this.availableRooms = list;
	}

	public JButton getChooseRoomB(){
		return chooseRoomB;
	}

	public JList getRoomList(){
		return availableRooms;
	}

	public BoardPanel getBoardPanel(){
		return boardPanel;
	}

	public JTextField getLoginF(){
		return loginF;
	}

	public JTextArea getPlayersList(){
		return playersList;
	}

	public JTextArea getChatInput(){
		return chatPanel.getInputArea();
	}

	public JTextArea getChatArea(){
		return chatPanel.getChatArea();
	}

	public JLabel getWaiting() { return waiting; }

	public void scrollDown() { chatPanel.scrollDown(); }

	public JMenuItem getSkip() {
		return skip;
	}

	public JMenuItem getAddBot() { return addBot; }
}
