package TP;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BoardPanel extends JPanel {

	/**
	 * Panel containing the board
	 */

	private int[][] board;
	private int[] widths;
	private Color[] colors = {Color.WHITE, Color.RED, Color.MAGENTA, Color.BLUE, Color.GREEN, Color.YELLOW, Color.ORANGE};
	//								0			1			2			3				4			5			6
	private int diameter;

	private ArrayList<Ellipse2D> fieldList;
	private int fieldListIndex;
	private int clickCount, highlightIndex;

	private Map<Ellipse2D, Point> fieldMap;

	private int windowHeight, windowWidth;

	public BoardPanel(int[][] b, int[] w, int height, int width) {
		this.board = b;
		this.widths = w;
		this.diameter = 30;
		this.fieldList = new ArrayList<Ellipse2D>();
		this.fieldMap = new HashMap<Ellipse2D, Point>();
		this.fieldListIndex = 0;
		this.windowHeight = height;
		this.windowWidth = width;
		this.clickCount = 0;
		this.highlightIndex = -1;

		for (int i = 0; i < 121; i++) {
			//initialize field list with ellipses
			this.fieldList.add(new Ellipse2D.Double());
		}
	}

	public ArrayList<Ellipse2D> getFieldList() {
		return fieldList;
	}

	public void setClickCount(int clickCount) {
		this.clickCount = clickCount;
	}

	public int getClickCount() {
		return this.clickCount;
	}

	public void setHighlightIndex(int highlightIndex) {
		this.highlightIndex = highlightIndex;
	}

	public int getHighlightIndex() {
		return this.highlightIndex;
	}


	public void setBoard(int[][] board){
		this.board = board;
	}

	/**
	 * Returns the corresponding x, y in the board, given an Ellipse2D object.
	 *
	 * @param ellipse given Ellipse2D object
	 * @return a 2D array with x and y coordinates
	 */
	public Point ellipseToIndex(Ellipse2D ellipse) {
		return fieldMap.get(ellipse);
	}

	/**
	 * Draws a row of fields of length widths[i].
	 *
	 * @param g2      drawing object
	 * @param xOffset offset in X axis
	 * @param yOffset offset in Y axis
	 * @param i       widths array index
	 */
	private void drawRow(Graphics2D g2, int xOffset, int yOffset, int i) {

		int from = 2, to = widths[i] - 1;

		if (i == 4 || i == 14) {
			from = 10;
			to = 18;
		}

		for (int j = from; j < to; j += 2) {

			//create a new ellipse and set it in the field list
			Ellipse2D piece = new Ellipse2D.Double(xOffset - (diameter / 2), yOffset - (diameter / 2), diameter, diameter);
			fieldList.set(fieldListIndex, piece);
			Point tempArr = new Point(i, j);
			fieldMap.put(piece, tempArr);

			//draw it
			if (fieldListIndex == highlightIndex) {
				Stroke oldStroke = g2.getStroke();
				g2.setStroke(new BasicStroke(3));
				Paint oldPaint = g2.getPaint();
				g2.setPaint(Color.YELLOW);
				g2.draw(piece);
				g2.setPaint(oldPaint);
				g2.setStroke(oldStroke);
			} else {
				g2.draw(piece);
			}
			Paint oldPaint = g2.getPaint();
			g2.setPaint(colors[board[i][j]]);
			g2.fill(piece);
			g2.setPaint(oldPaint);

			//add offset and increment index
			xOffset += diameter;
			fieldListIndex++;
		}
	}

	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D) g;

		int baseOffset = diameter / 2;

		int rowOffset, offsetCount;
		int x = windowWidth / 2;
		int y = diameter;

		fieldListIndex = 0;                                //set fieldListIndex back to 0

		for (int i = 1; i < 5; i++) {
			rowOffset = x - ((i - 1) * baseOffset);
			drawRow(g2, rowOffset, y, i);
			y += diameter;
		}

		offsetCount = 12;
		for (int i = 5; i < 10; i++) {
			rowOffset = x - (offsetCount * baseOffset);
			drawRow(g2, rowOffset, y, i);
			y += diameter;
			offsetCount--;
		}

		offsetCount = 9;
		for (int i = 10; i < 14; i++) {
			rowOffset = x - (offsetCount * baseOffset);
			drawRow(g2, rowOffset, y, i);
			y += diameter;
			offsetCount++;
		}

		for (int i = 14; i < 18; i++) {
			rowOffset = x - ((18 - i - 1) * baseOffset);
			drawRow(g2, rowOffset, y, i);
			y += diameter;
		}
	}
}
