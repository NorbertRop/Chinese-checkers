package TP;

public class ClientGameSession {
	private Board board;

	ClientGameSession(Board bd){
		this.board = bd;
	}

	public Board getBoard() {
		return board;
	}
}
