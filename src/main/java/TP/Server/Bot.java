package TP.Server;

import TP.Board;
import TP.Point;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Bot extends SocketThread {

	/**
	 * A simple bot that really likes board games.
	 */

	private static int number = 1;
	private final Object displayThreadLock = new Object();
	private boolean mIsBound;

	Bot(ArrayList<ServerGameSession> games, Map<String, ObjectOutputStream> map, ArrayList<String> players,
		ServerGameSession session, ArrayList<Bot> bots) {
		rooms = games;
		this.map = map;
		this.players = players;
		this.actualGame = session;
		clientNick = "Bot" + number;
		this.bots = bots;
		number++;
		possibleMoves = new ArrayList<Point>();
		mIsBound = true;
	}

	private Point[] botMove() {
		Point[] move = new Point[2];
		Random random = new Random();

		int[][] b = actualGame.getBoard().getTheBoard();
		ArrayList<Point> oppositeBase = Board.getWinPositions(color);
		ArrayList<Point> availablePieces = new ArrayList<Point>();

		for (int i = 1; i < 18; i++) {
			for (int j = 0; j < actualGame.getBoard().getWidths()[i]; j += 2) {
				if (b[i][j] == color) {
					//for each piece of my color check if there are any spots available
					//to which the piece could move
					Point p = new Point(i, j);
					checkMove(p, oppositeBase.get(0));
					if (possibleMoves.isEmpty())                                    //if piece is stuck, skip to the next one
						continue;
					availablePieces.add(p);
				}
			}
		}

		double minimum = Integer.MAX_VALUE;
		HashMap<Double, Point> distToPiece = new HashMap<Double, Point>();

		Point randomPoint = availablePieces.get(random.nextInt(availablePieces.size()));        //take a random point
		checkMove(randomPoint, oppositeBase.get(0));                                            //get its possible moves

		for (Point point : possibleMoves) {
			if (calculateDistance(point, oppositeBase.get(0)) < minimum) {
				minimum = calculateDistance(point, oppositeBase.get(0));
				distToPiece.put(calculateDistance(point, oppositeBase.get(0)), point);
			}
		}

		move[0] = randomPoint;
		move[1] = Board.getArrayPoint(distToPiece.get(minimum));

		return move;
	}

	private double calculateDistance(Point p1, Point p2) {
		return Math.sqrt(Math.pow((p2.getX() - p1.getX()), 2) + Math.pow(p2.getY() - p1.getY(), 2));
	}

	public void getMessage(String msg) {
		message = msg;
	}

	@Override
	public void run() {
		while (true) {
			try {
				synchronized (displayThreadLock) {
					if (!mIsBound) {
						try {
							displayThreadLock.wait(); // this thread will wait here util displayThreadLock.notify() is called
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}

				synchronized (this) {
					this.wait(500);
				}
				if (message != null) {
					//System.out.println(clientNick + " received - " + message);
					String[] args = message.split(" ");
					String type = args[0];

					if (type.equals("ready")) {
						createBoard();
						color = actualGame.registerColor(clientNick);
						if (actualGame.getTurn().equals(clientNick)) {
							synchronized (this) {
								this.wait(6000);
							}
							botAction();
						}
					} else if (type.equals("bye")) {
						break;
					} else if (type.equals("move")) {
						if (actualGame.getTurn().equals(clientNick)) {
							botAction();
						}
					} else if (type.equals("skip")) {
						if (actualGame.getTurn().equals(clientNick)) {
							botAction();
						}
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			message = null;
		}

	}

	public String getNick() {
		return clientNick;
	}

	private void botAction() {
		synchronized (this) {
			try {
				this.wait(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Point[] move = botMove();
		Point from = move[0];
		Point to = move[1];
		doMove(from, to);
		actualGame.nextPlayer();
		sendAll("move y " + from.getX() + " " + from.getY() + " " + to.getX() + " " + to.getY());
		sendTurn();
		if (checkWin()) {
			sendAll("end " + clientNick);
		}
	}

	private void sendAll(String msg) {
		for (Map.Entry<String, ObjectOutputStream> entry : map.entrySet()) {
			if (actualGame.getPlayersNick().contains(entry.getKey())) {
				try {
					entry.getValue().writeObject(msg);
				} catch (IOException e) {
					deletePlayer();
					e.printStackTrace();
				}
			}
		}
		for (Bot b : bots) {
			if (actualGame.getPlayersNick().contains(b.getNick()) && !b.getNick().equals(clientNick)) {
				synchronized (b) {
					b.unlock();
					b.getMessage(msg);
					b.lock();
				}
			}
		}
	}

	private void sendTurn() {
		for (Map.Entry<String, ObjectOutputStream> entry : map.entrySet()) {
			if (actualGame.getPlayersNick().contains(entry.getKey())) {
				try {
					entry.getValue().writeObject("chat INFO " + actualGame.getTurn() + " turn \n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void lock() {
		mIsBound = true;
	}

	public void unlock() {
		mIsBound = false;
		synchronized (displayThreadLock) {
			displayThreadLock.notify(); // call notify to break the wait, so the displayThread continues to run
		}
	}
}