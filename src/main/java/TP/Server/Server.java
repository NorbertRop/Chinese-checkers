package TP.Server;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class Server {
	private ServerSocket providerSocket;
	private ArrayList<ServerGameSession> rooms;
	private HashMap<String, ObjectOutputStream> map;
	private ArrayList<String> players;
	private ArrayList<Bot> bots;

	public Server() {
		createRooms();
		map = new HashMap<String, ObjectOutputStream>();
		players = new ArrayList<String>();
		bots = new ArrayList<Bot>();
	}

	private void run() {
		try {
			//Creating the socket
			providerSocket = new ServerSocket(2004, 10);
			System.out.println("Waiting for connection");
			Socket connection = providerSocket.accept();
			System.out.println("Connection received from " + connection.getInetAddress().getHostName());

			new SocketThread(connection, rooms, map, players, bots).start();
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			//Closing connection
			try {
				providerSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	public static void main(String args[]) {
		Server server = new Server();
		while (true) {
			server.run();
		}
	}

	public int getNumberOfPlayers(){
		return players.size();
	}

	private void createRooms() {
		rooms = new ArrayList<ServerGameSession>();

		rooms.add(new ServerGameSession("1# room 2 players",2));
		rooms.add(new ServerGameSession("2# room 4 players",4));
		rooms.add(new ServerGameSession("3# room 6 players",6));
	}
}