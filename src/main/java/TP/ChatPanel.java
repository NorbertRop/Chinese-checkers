package TP;

import javax.swing.*;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

public class ChatPanel extends JPanel{

    private JTextArea chatArea, inputArea;
    private JScrollPane scrollPane;

    ChatPanel(){

        setLayout(new GridBagLayout());

        this.chatArea = new JTextArea();   //5, 20
        this.inputArea = new JTextArea();  //3, 20

        inputArea.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "inputAction");

        chatArea.setEditable(false);
        chatArea.setLineWrap(true);
        chatArea.setWrapStyleWord(true);

        inputArea.setLineWrap(true);
        inputArea.setWrapStyleWord(true);

        scrollPane = new JScrollPane(chatArea);

        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.anchor = GridBagConstraints.LAST_LINE_END;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0; c.weighty = 1.0;
        add(scrollPane, c);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0; c.weighty = 0.0;
        add(inputArea, c);
    }

    /**
     * Scroll down the chat area to view newest messages
     * but allow user to scroll up to see older ones.
     */
    public void scrollDown(){
        final JScrollBar bar = scrollPane.getVerticalScrollBar();
        AdjustmentListener listener = new AdjustmentListener() {
            public void adjustmentValueChanged(AdjustmentEvent e) {
                Adjustable a = e.getAdjustable();
                a.setValue(a.getMaximum());
                bar.removeAdjustmentListener(this);
            }
        };
        bar.addAdjustmentListener(listener);
    }

	public JTextArea getInputArea() {
		return inputArea;
	}

	public JTextArea getChatArea() {
		return chatArea;
	}
}
